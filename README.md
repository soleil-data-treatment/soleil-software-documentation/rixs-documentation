# Aide et ressources de RIXS pour Synchrotron SOLEIL

## Résumé

- Triage et visualization des spectres 1D: XAS, scan positions, etc. (Cahier des charges détaillé envoyé a S. Bac en avril 2020)
Visualisation des donnais contextuels (énergie et polarization de la ligne, résolution, positions échantillon et spectromètre)
Traitement des images CCD pour extraire les spectres RIXS: application des seuils, extraction de la courbure (par fit gaussien) nécessaire pour additionner les lignes de la CCD après interpolation linéaire.
Visualisation des spectres RIXS et construction des cartes RIXS en deux représentations: Energie d’emission et Perte d’énergie.
- Créé à Synchrotron Soleil

## Sources

- Code source: sur un dépôt interne a Soleil
- Documentation officielle: à voir avec la ligne SEXTANTS

## Installation

- Systèmes d'exploitation supportés: Windows,  igor
- Installation: Facile (tout se passe bien),  igor est chers

## Format de données

- en entrée: nexus
- en sortie: spectre 1D,  wave 3D
- sur un disque dur,  sur la Ruche
